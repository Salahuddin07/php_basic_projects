<?php
    if($_SERVER["REQUEST_METHOD"]=="POST"){
        $first_name=$_POST["first_name"];
        $last_name=$_POST["last_name"];
        $email = $_POST["email"];
        $password = $_POST["password"];
        $password_again = $_POST["password_again"];




        echo $first_name;
        echo $last_name;
        echo $email;
        echo $password;
        echo $password_again;

        
    }
   

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="./css/bootstrap.min.css">
    <link rel="stylesheet" href="./style.css">

</head>

<body class="main_content">

    <div class="container mt-5">
        <div class="row ">
            <div class="col-4">

            </div>
            <div class="col-4 mt-5 main_form text-white">
                <form action="" method="post">
                <div class="form-group mt-2">
                    <label for="first_name" class="form-label">First Name: </label>
                    <input type="text" name="first_name" id="first_name" class="form-control">
                </div>
                <div class="form-group">
                    <label for="last_name" class="form-label">Last Name: </label>
                    <input type="text" name="last_name" id="last_name" class="form-control">
                </div>
                <div class="form-group">
                    <label for="email" class="form-label">Email : </label>
                    <input type="text" name="email" id="email" class="form-control">
                </div>
                <div class="form-group">
                    <label for="password" class="form-label">Password : </label>
                    <input type="text" name="password" id="password" class="form-control">
                </div>
                <div class="form-group">
                    <label for="password_again" class="form-label">Password again : </label>
                    <input type="text" name="password_again" id="password_again" class="form-control">
                </div>
                <div class="d-flex justify-content-center text-white mt-5 mb-3">
                    <button class="btn btn-success" type="submit">Sign Up</button>
                </div>
                </form>
            </div>
            <div class="col-md-4">

            </div>
        </div>
    </div>

    <script src="./js/bootstrap.min.js"></script>
</body>

</html>